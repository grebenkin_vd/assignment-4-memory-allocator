#define _DEFAULT_SOURCE

#include "mem.h"
#include "testlib.h"
#include <stdio.h>
#include <string.h>


#define PAGE_SIZE 4096
#define HEAP_INITIAL_SIZE 1
#define HEAP_PAGES_SIZE 8192
#define HEAP_PAGES_SIZE_BIG 20480
#define RUSSIAN_SALARY 15000
#define ALLOC_SIZE 32
#define ALLOC_SIZE_BIG 64

bool test0() {
  printf("Тест 0. Обычное успешное выделение памяти.\n");
  heap_init(HEAP_INITIAL_SIZE);
  void* alloc = _malloc(ALLOC_SIZE);
  if (!alloc) return false;
  debug_heap(stdout, HEAP_START);
  _free(alloc);
  munmap(HEAP_START, HEAP_PAGES_SIZE);
  return true;
}

bool test1() {
  printf("\nТест 1. Освобождение одного блока из нескольких выделенных.\n");
  heap_init(HEAP_INITIAL_SIZE);
  void* alloc1 = _malloc(ALLOC_SIZE);
  if (!alloc1) return false;
  void* alloc2 = _malloc(ALLOC_SIZE_BIG);
  if (!alloc2) return false;
  debug_heap(stdout, HEAP_START);
  _free(alloc1);
  debug_heap(stdout, HEAP_START);
  _free(alloc2);
  munmap(HEAP_START, HEAP_PAGES_SIZE);
  return true;
}

bool test2() {
  printf("\nТест 2. Освобождение двух блоков из нескольких выделенных. \n");
  heap_init(HEAP_INITIAL_SIZE);
  void* alloc1 = _malloc(ALLOC_SIZE);
  if (!alloc1) return false;
  void* alloc2 = _malloc(ALLOC_SIZE_BIG);
  if (!alloc2) return false;
  void* alloc3 = _malloc(ALLOC_SIZE);
  if (!alloc3) return false;
  debug_heap(stdout, HEAP_START);
  _free(alloc1);
  _free(alloc3);
  debug_heap(stdout, HEAP_START);
  _free(alloc2);
  munmap(HEAP_START, HEAP_PAGES_SIZE);
  return true;
}

bool test3() {
  printf("\nТест 3. Память закончилась, новый регион памяти расширяет старый. \n");
  heap_init(HEAP_INITIAL_SIZE);
  debug_heap(stdout, HEAP_START);
  void* alloc1 = _malloc(HEAP_PAGES_SIZE);
  if (!alloc1) return false;
  debug_heap(stdout, HEAP_START);
  _free(alloc1);
  munmap(HEAP_START, HEAP_PAGES_SIZE_BIG);
  return true;
}

bool test4() {
  printf("\nТест 4. Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте. \n");
  heap_init(HEAP_INITIAL_SIZE);
  (void) mmap(HEAP_START + HEAP_PAGES_SIZE, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
  debug_heap(stdout, HEAP_START);
  void* alloc1 = _malloc(RUSSIAN_SALARY);
  if (!alloc1) return false;
  debug_heap(stdout, HEAP_START);
  _free(alloc1);
  return true;
}

void run_test(bool (*test)()) {
    printf("test Passed, status = "); 
  if (test()) 
  {
    printf("OK\n"); // Kostya Dushnila
  } 
  else 
  {
    printf("ERROR\n"); 
  }
}

void start_pipeline() {
    run_test(test0);
    run_test(test1);
    run_test(test2);
    run_test(test3);
    run_test(test4);
    printf("Pipline complete");
}
